<?php

/**
 * @file
 * Hooks and helper functions for the ga_popular_nodes module.
 */

use Drupal\ga_popular_nodes\CronRunner;
use Drupal\ga_popular_nodes\DataFetcher\GoogleAnalyticsDataFetcher;
use Drupal\ga_popular_nodes\PathMapper\ExternalToInternalPathMapper;

/* Hooks. */

/**
 * Implements hook_views_api().
 */
function ga_popular_nodes_views_api() {
  return array(
    'api' => 3.0,
  );
}

/**
 * Implements hook_permission().
 */
function ga_popular_nodes_permission() {
  $permissions = array();

  // Configure global ga_popular_nodes settings.
  $permissions['administer ga_popular_nodes'] = array(
    'title' => t('Administer Google Analytics: popular nodes'),
  );

  return $permissions;
}

/**
 * Implements hook_menu().
 */
function ga_popular_nodes_menu() {
  $items = array();

  // Configure global ga_popular_nodes settings.
  $items['admin/config/services/ga_popular_nodes'] = array(
    'title' => 'Google Analytics: popular nodes',
    'description' => 'Configure the Google Analytics: popular nodes module.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('_ga_popular_nodes_config'),
    'access arguments' => array('administer ga_popular_nodes'),
    'file' => 'ga_popular_nodes.admin.inc',
  );
  $items['admin/config/services/ga_popular_nodes/settings'] = array(
    'title' => 'Settings',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  // A manual query form.
  $items['admin/config/services/ga_popular_nodes/manual-query'] = array(
    'title' => 'Manual query',
    'description' => 'Test the Google Analytics: popular nodes module by manually querying Google Analytics and displaying the results.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('_ga_popular_nodes_manual_query'),
    'access arguments' => array('administer ga_popular_nodes'),
    'file' => 'ga_popular_nodes.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Implements hook_cron().
 */
function ga_popular_nodes_cron() {
  // Prepare a cron runner, then run it.
  $fetcher = new GoogleAnalyticsDataFetcher();
  $mapper = new ExternalToInternalPathMapper();
  $query_params = _ga_popular_nodes_query_parameters();
  $query_runner = new CronRunner($fetcher, $mapper, $query_params);
  $query_runner->run();
}

/* Helper functions. */

/**
 * Helper function to return default Google Analytics query parameters.
 *
 * @return array
 *   An associative array of query parameters.
 */
function _ga_popular_nodes_query_parameters() {
  // Defaults for the admin's preferences.
  $preference_defaults = array(
    'start_date' => '30daysAgo',
    'end_date' => 'yesterday',
    'max_results' => 50,
  );

  // The admin's actual preferences.
  $preferences = variable_get('ga_popular_nodes_query_parameters', array());

  // Query parameters that cannot be overridden by the admin, because the code
  // in this module depends on these assumptions.
  $unchangeable = array(
    'metrics' => 'ga:uniquePageviews',
    'dimensions' => 'ga:pagePath',
    'sort_metric' => '-ga:uniquePageviews',
    'samplingLevel' => 'FASTER',
    'include-empty-rows' => FALSE,
    'start_index' => 1,
    'output' => 'json',
    'prettyPrint' => FALSE,
  );

  // Note that array_merge() is left-associative. That is to say, if the input
  // arrays have the same string keys, then the later value for that key will
  // overwrite the previous one.
  //
  // What happens here is, the defaults are overridden by the saved preferences,
  // which are then overridden by whichever query parameters can't be changed.
  $query_parameters = array_merge($preference_defaults, $preferences, $unchangeable);

  return $query_parameters;
}
