<?php

/**
 * @file
 * Views module hooks for the ga_popular_nodes module.
 */

/**
 * Implements hook_views_data().
 */
function ga_popular_nodes_views_data() {
  $data = array();

  // Describe the ga_popular_nodes_data table to Views.
  $data['ga_popular_nodes_data']['table']['group'] = t('Google Analytics popular nodes');
  $data['ga_popular_nodes_data']['table']['base'] = array(
    'title' => t('Google Analytics popular nodes cache'),
    'help' => t('Parsed and cached Google Analytics data on the most popular nodes on the site.'),
    'field' => 'nid',
    'access query tag' => 'node_access',
    'defaults' => array(
      'field' => 'unique_pageviews',
    ),
  );
  $data['ga_popular_nodes_data']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  $data['ga_popular_nodes_data']['updated'] = array(
    'title' => t('Updated'),
    'help' => t('When this entry was last updated.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  $data['ga_popular_nodes_data']['unique_pageviews'] = array(
    'title' => t('Unique pageviews'),
    'help' => t('The number of unique pageviews for this node.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['ga_popular_nodes_data']['nid'] = array(
    'title' => t('Associated node'),
    'help' => t('The node that this entry refers to.'),
    'relationship' => array(
      'base' => 'node',
      'base field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Associated node'),
      'title' => t('Associated node'),
      'help' => t('The node that this entry refers to.'),
    ),
  );

  return $data;
}
