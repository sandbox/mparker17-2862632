<?php

/**
 * @file
 * Administration functions for the ga_popular_nodes module.
 */

use Drupal\ga_popular_nodes\CronRunner;
use Drupal\ga_popular_nodes\DataFetcher\DummyDataFetcher;
use Drupal\ga_popular_nodes\DataFetcher\GoogleAnalyticsDataFetcher;
use Drupal\ga_popular_nodes\PathMapper\ExternalToInternalPathMapper;

/**
 * Form constructor for the global ga_popular_nodes settings config form.
 *
 * @see ga_popular_nodes_menu()
 *
 * @ingroup forms
 */
function _ga_popular_nodes_config($form, &$form_state) {
  // Tell the user if they are not authorized with Google Analytics and where to
  // find the authorization form and settings.
  $access_token = variable_get('google_analytics_reports_api_access_token', 0);
  $form['google_analytics_reports_api_status'] = array(
    '#type' => 'item',
    '#title' => t('Google Analytics Reports API'),
    '#markup' => ($access_token) ? t('Authorized and ready to use') : t('Not authorized to use your Google Analytics account. You must authorize Drupal to use your Google Analytics account for this module to work.'),
    '#description' => t('Authorize or change settings from <a href="@url">the Google Analytics Reports API configuration page</a>.', array(
      '@url' => url('admin/config/system/google-analytics-reports-api'),
    )),
  );

  // Allow administrators to blacklist certain nodes, such as the front page of
  // the site.
  $form['ga_popular_nodes_blacklist'] = array(
    '#type' => 'textarea',
    '#title' => t('Blacklisted node IDs'),
    '#description' => t('These nodes will not be included in the list of popular content. If the front page of your site is a node, you probably want to add it to this list. Put each node ID on a separate line or separate them by commas.'),
    '#default_value' => variable_get('ga_popular_nodes_blacklist', ''),
  );

  // Allow users to override query parameters. We define this fieldset as a tree
  // so that data entered in fields inside it are stored as an associative
  // array.
  $query_parameter_defaults = _ga_popular_nodes_query_parameters();
  $form['ga_popular_nodes_query_parameters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Query parameters'),
    '#description' => t('Because the API changes occasionally, these parameters will be passed directly to Google: Drupal will not check that what you enter here is valid. For more information about these parameters and how they are used, please see the Google Analytics <a href="@reference_url">Core Reporting API, version 3 - Reference Guide: Query Parameters Summary</a>. To test a query, use the <a href="@query_explorer_url">Google Analytics Query Explorer</a>.', array(
      '@reference_url' => url('https://developers.google.com/analytics/devguides/reporting/core/v3/reference'),
      '@query_explorer_url' => url('https://ga-dev-tools.appspot.com/query-explorer/'),
    )),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );
  $form['ga_popular_nodes_query_parameters']['start_date'] = array(
    '#type' => 'textfield',
    '#title' => t('Start date'),
    '#description' => t('Start date for fetching Analytics data. Requests can specify a start date formatted as <strong>YYYY-MM-DD</strong>, or as a relative date (e.g., <strong>today</strong>, <strong>yesterday</strong>, or <strong>NdaysAgo</strong> where <strong>N</strong> is a positive integer).'),
    '#default_value' => $query_parameter_defaults['start_date'],
  );
  $form['ga_popular_nodes_query_parameters']['end_date'] = array(
    '#type' => 'textfield',
    '#title' => t('End date'),
    '#description' => t('End date for fetching Analytics data. Request can specify an end date formatted as <strong>YYYY-MM-DD</strong>, or as a relative date (e.g., <strong>today</strong>, <strong>yesterday</strong>, or <strong>NdaysAgo</strong> where <strong>N</strong> is a positive integer).'),
    '#default_value' => $query_parameter_defaults['end_date'],
  );
  $form['ga_popular_nodes_query_parameters']['max_results'] = array(
    '#type' => 'textfield',
    '#title' => t('Max results'),
    '#description' => t('The maximum number of rows to include in the response.'),
    '#default_value' => $query_parameter_defaults['max_results'],
  );
  $form['ga_popular_nodes_query_parameters']['other_fields'] = array(
    '#type' => 'item',
    '#title' => t('Query parameters set automatically'),
    '#description' => t('For technical reasons, these query parameters are not configurable.'),
    '#markup' => theme('item_list', array('items' => array(
      'ids',
      'metrics',
      'dimensions',
      'sort',
      'filters',
      'segment',
      'samplingLevel',
      'include-empty-rows',
      'start-index',
      'output',
      'fields',
      'prettyPrint',
      'userIp',
      'quotaUser',
      'access_token',
      'callback',
      'key',
    ))),
  );

  return system_settings_form($form);
}

/**
 * Form constructor for a manual query form.
 *
 * @see ga_popular_nodes_menu()
 *
 * @ingroup forms
 */
function _ga_popular_nodes_manual_query($form, &$form_state) {
  // This form will eventually display data, but the user has to click the "run"
  // button first. If they haven't done that yet, we still need to initialize
  // the variable.
  $form_state['build_info']['args']['data_to_display'] = isset($form_state['build_info']['args']['data_to_display'])
    ? (array) $form_state['build_info']['args']['data_to_display']
    : array();

  // Let the user choose which fetcher to use.
  if (variable_get('google_analytics_reports_api_access_token', 0)) {
    $form['data_fetcher']['googleanalytics'] = array(
      '#type' => 'radio',
      '#title' => t('Google Analytics'),
      '#description' => t('Gets data from Google Analytics. This is probably what you want to use.'),
      '#return_value' => 'googleanalytics',
      '#parents' => array('data_fetcher'),
    );
  }
  $form['data_fetcher']['dummy'] = array(
    '#type' => 'radio',
    '#title' => t('Dummy data fetcher'),
    '#description' => t('The dummy data fetcher simulates a response from Google Analytics using hard-coded data. This is only useful if the site is not authorized to use data from your Google Analytics account. WARNING: This will update the database with the dummy data!'),
    '#return_value' => 'dummy',
    '#parents' => array('data_fetcher'),
  );

  // Set the first available data fetcher as the default.
  reset($form['data_fetcher']);
  $first_data_fetcher = key($form['data_fetcher']);
  $form['data_fetcher'][$first_data_fetcher]['#default_value'] = TRUE;

  // Display a "run" button.
  $form['actions']['run'] = array(
    '#type' => 'submit',
    '#value' => t('Run a manual query'),
    '#submit' => array('_ga_popular_nodes_manual_query_run'),
  );

  // Display a container for the data to display.
  $form['data_to_display'] = array(
    '#type' => 'fieldset',
    '#title' => t('Data returned by Google Analytics'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  // Initial state: the user has not clicked the "run" button yet.
  if (empty($form_state['build_info']['args']['data_to_display'])) {
    $form['data_to_display']['no_data'] = array(
      '#markup' => t('A query has not been run yet, so there is no data to display yet.'),
    );
  }
  // Final state: the user has clicked the "run" button.
  else {
    // Show the query parameters that we passed to Google Analytics.
    $form['data_to_display']['query_params'] = array(
      '#type' => 'item',
      '#title' => t('Query parameters passed to Google Analytics'),
      '#markup' => '<pre>' . check_plain(var_export($form_state['build_info']['args']['data_to_display']['query_params'], TRUE)) . '</pre>',
    );

    // Show the raw data returned from Google Analytics.
    $form['data_to_display']['raw_ga_data'] = array(
      '#type' => 'item',
      '#title' => t('Raw Google Analytics data'),
      '#markup' => '<pre>' . check_plain(var_export($form_state['build_info']['args']['data_to_display']['raw'], TRUE)) . '</pre>',
    );

    // Show nodes caught by the blacklist.
    $form['data_to_display']['nodes_caught_by_blacklist'] = array(
      '#type' => 'item',
      '#title' => t('Node IDs caught by the blacklist'),
      '#markup' => '<pre>' . check_plain(var_export($form_state['build_info']['args']['data_to_display']['nodes_caught_by_blacklist'], TRUE)) . '</pre>',
    );

    // Show the corresponding internal paths.
    $form['data_to_display']['internal_paths'] = array(
      '#type' => 'item',
      '#title' => t('Node IDs mapped to pageviews'),
      '#markup' => '<pre>' . check_plain(var_export($form_state['build_info']['args']['data_to_display']['internal_paths'], TRUE)) . '</pre>',
    );

    // Show the entries inserted into the database.
    $form['data_to_display']['entries'] = array(
      '#type' => 'item',
      '#title' => t('Data written to the database'),
      '#markup' => '<pre>' . check_plain(var_export($form_state['build_info']['args']['data_to_display']['entries'], TRUE)) . '</pre>',
    );
  }

  return $form;
}

/**
 * Form handler for _ga_popular_nodes_manual_query().
 *
 * Queries Google Analytics and returns some useful data.
 */
function _ga_popular_nodes_manual_query_run($form, &$form_state) {
  // Ensure the form state we're going to return is initalized.
  $form_state['build_info']['args']['data_to_display'] = array();

  // Prepare a cron runner, then run it.
  $fetcher = ($form_state['values']['data_fetcher'] === 'dummy')
    ? new DummyDataFetcher()
    : new GoogleAnalyticsDataFetcher();
  $mapper = new ExternalToInternalPathMapper();
  $query_params = _ga_popular_nodes_query_parameters();
  $query_runner = new CronRunner($fetcher, $mapper, $query_params);
  $query_runner->run();

  // Pass the debugging data back to the page.
  $form_state['build_info']['args']['data_to_display'] = $query_runner->getDebugData();

  // Tell Drupal to rebuild the form state instead of discarding it.
  $form_state['rebuild'] = TRUE;
}
