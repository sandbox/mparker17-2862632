<?php

namespace Drupal\ga_popular_nodes\Model;

/**
 * A class to model our cache of popular node data.
 */
class GaPopularNodesData {

  /**
   * The timestamp when this entry was last updated.
   *
   * @var int
   */
  public $updated;

  /**
   * The number of unique pageviews for this entry.
   *
   * @var int
   */
  public $uniquePageviews;

  /**
   * The node ID for this entry.
   *
   * @var int
   */
  public $nid;

  /**
   * GaPopularNodesData constructor.
   *
   * @param int $nid
   *   The node ID for this entry.
   * @param int $unique_pageviews
   *   The timestamp when this entry was last updated.
   * @param int $updated
   *   The number of unique pageviews for this entry.
   */
  public function __construct($nid, $unique_pageviews = 0, $updated = 0) {
    $this->nid = $nid;
    $this->uniquePageviews = $unique_pageviews;
    $this->updated = $updated;
  }

  /**
   * Save this entry.
   */
  public function save() {
    // Re-set when this entry was last updated.
    $this->updated = time();

    // Issue a merge query to insert or update the entry.
    db_merge('ga_popular_nodes_data')
      ->key(array(
        'nid' => $this->nid,
      ))
      ->fields(array(
        'updated' => $this->updated,
        'unique_pageviews' => $this->uniquePageviews,
      ))
      ->execute();
  }

}
