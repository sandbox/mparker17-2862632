<?php

namespace Drupal\ga_popular_nodes;

use Drupal\ga_popular_nodes\DataFetcher\DataFetcherInterface;
use Drupal\ga_popular_nodes\Model\GaPopularNodesData;
use Drupal\ga_popular_nodes\PathMapper\PathMapperInterface;

/**
 * A class to perform the actions we would normally take on a cron run.
 *
 * Splitting this into a class allows us to use the same code in both
 * hook_cron() and on the manual query page.
 */
class CronRunner {

  /**
   * Raw data returned by Google Analytics.
   *
   * @var array
   */
  public $rawAnalyticsData;

  /**
   * Internal paths mapped to pageviews.
   *
   * @var array
   */
  public $internalPageviews;

  /**
   * Debugging data.
   *
   * @var array
   */
  protected $debugData;

  /**
   * A Data Fetcher to use.
   *
   * @var \Drupal\ga_popular_nodes\DataFetcher\DataFetcherInterface
   */
  public $fetcher;

  /**
   * A Path Mapper to use.
   *
   * @var \Drupal\ga_popular_nodes\PathMapper\PathMapperInterface
   */
  public $mapper;

  /**
   * An associative array of query parameters to pass to Google Analytics.
   *
   * @var array
   */
  public $queryParams;

  /**
   * CronRunner constructor.
   *
   * @param \Drupal\ga_popular_nodes\DataFetcher\DataFetcherInterface $fetcher
   *   A Data Fetcher to use.
   * @param \Drupal\ga_popular_nodes\PathMapper\PathMapperInterface $mapper
   *   A Path Mapper to use.
   * @param array $query_params
   *   An associative array of query parameters to pass to Google Analytics.
   */
  public function __construct(DataFetcherInterface $fetcher, PathMapperInterface $mapper, array $query_params) {
    $this->fetcher = $fetcher;
    $this->mapper = $mapper;
    $this->queryParams = $query_params;

    $this->rawAnalyticsData = array();
    $this->internalPageviews = array();
    $this->debugData = array(
      'nodes_caught_by_blacklist' => array(),
      'entries' => array(),
    );
  }

  /**
   * Run the query.
   */
  public function run() {
    $this->rawAnalyticsData = $this->fetcher->fetchAnalyticsDataAssoc($this->queryParams);

    $blacklisted_nodes = variable_get('ga_popular_nodes_blacklist', '');

    // First pass: Resolve paths given to us by Google Analytics to node IDs,
    // remove blacklisted nodes, and combine duplicates.
    foreach ($this->rawAnalyticsData as $analytics_datum) {
      $external_path = $analytics_datum['ga:pagePath'];

      // Find Drupal's internal path information.
      $router_item = $this->mapper->mapPath($external_path);
      $internal_path = $router_item['href'];

      // Skip if this is not a node view page.
      if (preg_match('#^node/[\d]+(/view)?$#', $internal_path) !== 1) {
        continue;
      }

      // Find the nid.
      $nid = $router_item['original_map'][1];

      // Skip if this node is blacklisted.
      $pattern = '#\b' . preg_quote($nid) . '\b#';
      if (preg_match($pattern, $blacklisted_nodes) === 1) {
        $this->debugData['nodes_caught_by_blacklist'][] = $nid;
        continue;
      }

      // Path aliases make it possible for two or more external paths to point
      // to the same internal path. To handle this, we store the nid hashed to
      // the number of pageviews. If we come across a duplicate, we add the
      // current number of pageviews to the old value. This means we have to
      // save the data in a separate pass.
      $old_pageviews = isset($this->internalPageviews[$nid])
        ? $this->internalPageviews[$nid]
        : 0;
      $this->internalPageviews[$nid] = $old_pageviews + $analytics_datum['ga:uniquePageviews'];
    }

    // Second pass: Save the data in a separate pass.
    foreach ($this->internalPageviews as $nid => $pageviews) {
      // Save this data.
      $entry = new GaPopularNodesData($nid, $pageviews);
      $entry->save();
      $this->debugData['entries'][] = $entry;
    }
  }

  /**
   * Return debugging data from the last run.
   *
   * @return array
   *   An associative array of debugging data.
   */
  public function getDebugData() {
    return $this->debugData + array(
      'query_params' => $this->queryParams,
      'raw' => $this->rawAnalyticsData,
      'internal_paths' => $this->internalPageviews,
    );
  }

}
