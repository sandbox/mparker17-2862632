<?php

namespace Drupal\ga_popular_nodes\PathMapper;

/**
 * PathMapper to convert an external URL to an internal path.
 */
class ExternalToInternalPathMapper implements PathMapperInterface {

  /**
   * A preg_replace() pattern to remove the base path from a string.
   *
   * @var string
   */
  protected $basePathRemovalPattern;

  /**
   * The path to the front page of the site.
   */
  protected $frontPagePath;

  /**
   * Whether the redirect module is enabled.
   *
   * @var bool
   */
  protected $redirectModuleEnabled;

  /**
   * An array of enabled language codes.
   *
   * @var array
   */
  protected $enabledLanguageCodes;

  /**
   * ExternalToInternalPathMapper constructor.
   */
  public function __construct() {
    // Prepare a preg_replace() pattern to remove the base path from a string.
    $base_path = base_path();
    $this->basePathRemovalPattern = '#^' . preg_quote($base_path, '#') . '#';

    // Load the path to the site front page.
    $this->frontPagePath = variable_get('site_frontpage', 'node');

    // Determine if the redirect module is enabled.
    $this->redirectModuleEnabled = module_exists('redirect');

    // Cache a list of enabled langauge codes.
    $enabled_languages = language_list();
    $this->enabledLanguageCodes = array_keys($enabled_languages);
  }

  /**
   * {@inheritdoc}
   */
  public function mapPath($external_path) {
    // Ensure $external_path is a string.
    $external_path = (string) $external_path;

    // Remove the base path from the beginning of the external path.
    $external_path = preg_replace($this->basePathRemovalPattern, '', $external_path, 1);

    // Special case: if the external path is empty after removing the base path,
    // then it is the front page of the site. During a regular bootstrap,
    // drupal_path_initialize() does this.
    if (empty($external_path)) {
      $external_path = $this->frontPagePath;
    }

    // If the redirect module is enabled, see if we can load a redirect for this
    // path. Since time is of the essence during a cron run, stop at the first
    // redirect we find for any language.
    if ($this->redirectModuleEnabled) {
      foreach ($this->enabledLanguageCodes as $lang_code) {
        $redirect = redirect_load_by_source($external_path, $lang_code);
        if ($redirect !== FALSE) {
          $external_path = $redirect->redirect;
          break;
        }
      }
    }

    // Resolve path aliases.
    $external_path = drupal_get_normal_path($external_path);

    // Resolve the external path to an internal router item.
    return menu_get_item($external_path);
  }

}
