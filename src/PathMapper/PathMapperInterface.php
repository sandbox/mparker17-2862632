<?php

namespace Drupal\ga_popular_nodes\PathMapper;

/**
 * An interface for Path Mapper objects.
 */
interface PathMapperInterface {

  /**
   * Map an external URL to an internal path.
   *
   * @param string $external_path
   *   The external URL.
   *
   * @return array|false
   *   A router item returned by menu_get_item().
   */
  public function mapPath($external_path);

}
