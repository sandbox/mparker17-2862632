<?php

namespace Drupal\ga_popular_nodes\DataFetcher;

/**
 * A class to get data from Google Analytics.
 */
class GoogleAnalyticsDataFetcher extends DataFetcherBase implements DataFetcherInterface {

  /**
   * {@inheritdoc}
   */
  public function fetchRawData(array $report_parameters) {
    // Get the data we need from Google Analytics.
    // Note that the GoogleAnalyticsReportsApiFeed object takes care of caching
    // data it retrieves for an appropriate length of time; we don't have to
    // worry about that here.
    return google_analytics_reports_api_report_data($report_parameters);
  }

}
