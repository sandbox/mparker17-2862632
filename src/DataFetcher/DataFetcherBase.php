<?php

namespace Drupal\ga_popular_nodes\DataFetcher;

use GoogleAnalyticsReportsApiFeed;
use InvalidArgumentException;
use Drupal\ga_popular_nodes\GaAuthenticationError;

/**
 * A basic data fetcher which knows how to read GA data into a useful format.
 */
abstract class DataFetcherBase implements DataFetcherInterface {

  /**
   * {@inheritdoc}
   */
  public function fetchAnalyticsDataAssoc(array $report_parameters) {
    $mapped_data = array();

    $response = $this->fetchRawData($report_parameters);

    // Check for authentication errors.
    if (is_array($response) && isset($response['error']) && $response['error'] === TRUE) {
      throw new GaAuthenticationError();
    }

    // Check for errors returned from Google Analytics.
    if (is_object($response) && isset($response->error) && !empty($response->error)) {
      throw new InvalidArgumentException($response->error);
    }

    // If we get here, then we have a valid set of Google Analytics data, but
    // we need to strip away the extra stuff and format the data in a way that
    // is easier to work with.
    $rows = isset($response->results->rows) ? $response->results->rows : array();
    $columns = $this->extractColumnsFromResponse($response);
    $mapped_data = $this->mapRowsAndColumns($rows, $columns);

    return $mapped_data;
  }

  /**
   * Extract a list of column headers from a Google Analytics response.
   *
   * @param \GoogleAnalyticsReportsApiFeed $response
   *   The response object to parse.
   *
   * @return array
   *   An array of column header names.
   */
  protected function extractColumnsFromResponse(GoogleAnalyticsReportsApiFeed $response) {
    $columns = array();

    if (isset($response->results->columnHeaders)) {
      foreach ($response->results->columnHeaders as $column_info) {
        $columns[] = $column_info->name;
      }
    }

    return $columns;
  }

  /**
   * Make an array of rows with meaningful column names.
   *
   * @param array $rows
   *   An array of rows.
   * @param array $columns
   *   An array of column names.
   *
   * @return array
   *   An array of rows. Each row is an associative array of column names mapped
   *   to the data for that row.
   */
  protected function mapRowsAndColumns(array $rows, array $columns) {
    $result = array();

    foreach ($rows as $row) {
      $result[] = array_combine($columns, $row);
    }

    return $result;
  }

}
