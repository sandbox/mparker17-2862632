<?php

namespace Drupal\ga_popular_nodes\DataFetcher;

/**
 * An interface for Google Analytics Data Fetcher objects.
 */
interface DataFetcherInterface {

  /**
   * Fetch Google Analytics data.
   *
   * @param array $report_parameters
   *   An array of report parameters.
   *
   * @return array
   *   An array of report data returned from Google Analytics.
   *
   * @throws \Drupal\ga_popular_nodes\GaAuthenticationError
   *   Throws a GaAuthenticationError if we couldn't connect to Google
   *   Analytics.
   *
   * @throws \InvalidArgumentException
   *   Throws a InvalidArgumentException if Google Analytics itself reported an
   *   error of some kind.
   */
  public function fetchAnalyticsDataAssoc(array $report_parameters);

  /**
   * Fetch raw Google Analytics data.
   *
   * @param array $report_parameters
   *   An array of report parameters.
   *
   * @return \GoogleAnalyticsReportsApiFeed
   *   A GoogleAnalyticsReportsApiFeed object containing the data returned by
   *   Google Analytics.
   */
  public function fetchRawData(array $report_parameters);

}
