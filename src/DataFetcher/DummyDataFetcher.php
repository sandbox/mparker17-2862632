<?php

namespace Drupal\ga_popular_nodes\DataFetcher;

/**
 * A dummy data fetcher to return hard-coded data for testing purposes.
 */
class DummyDataFetcher extends DataFetcherBase implements DataFetcherInterface {

  /**
   * {@inheritdoc}
   */
  public function fetchRawData(array $report_parameters) {
    $data = new \GoogleAnalyticsReportsApiFeed();

    // Set some dummy data.
    $data->response = new \stdClass();
    $data->response->request = <<<REQUEST
POST /analytics/v3/data/ga HTTP/1.1
Host: example.com
Content-Type: application/x-www-form-urlencoded
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-encoding: gzip, deflate, sdch, br
Accept-language: en-US,en;q=0.8
Cache-control: max-age=0
Dnt: 1
If-none-match: "1234567890ABCDEFGHIJKLMNOPQ/RSTUVWXYZabcdefghijklmnopqr"
Upgrade-insecure-requests: 1
User-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36
X-client-data: 1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcd

ids=ga:12345678&dimensions=ga:pagePath&metrics=ga:uniquePageviews&sort=-ga:uniquePageviews&start-date=30daysAgo&end-date=yesterday&max-results=50
REQUEST;
    $data->response->code = 200;
    $data->response->protocol = 'HTTP/1.1';
    $data->response->status_message = 'OK';
    $data->response->headers = array();
    $data->response->data = <<<RESPONSE
{
  "kind": "analytics#gaData",
  "id": "https://www.googleapis.com/analytics/v3/data/ga?ids=ga:12345678&dimensions=ga:pagePath&metrics=ga:uniquePageviews&sort=-ga:uniquePageviews&start-date=30daysAgo&end-date=yesterday&max-results=50",
  "query": {
    "start-date": "30daysAgo",
    "end-date": "yesterday",
    "ids": "ga:12345678",
    "dimensions": "ga:pagePath",
    "metrics": [
      "ga:uniquePageviews"
    ],
    "sort": [
      "-ga:uniquePageviews"
    ],
    "start-index": 1,
    "max-results": 50
  },
  "itemsPerPage": 50,
  "totalResults": 106,
  "selfLink": "https://www.googleapis.com/analytics/v3/data/ga?ids=ga:12345678&dimensions=ga:pagePath&metrics=ga:uniquePageviews&sort=-ga:uniquePageviews&start-date=30daysAgo&end-date=yesterday&max-results=50",
  "nextLink": "https://www.googleapis.com/analytics/v3/data/ga?ids=ga:12345678&dimensions=ga:pagePath&metrics=ga:uniquePageviews&sort=-ga:uniquePageviews&start-date=30daysAgo&end-date=yesterday&start-index=51&max-results=50",
  "profileInfo": {
    "profileId": "12345678",
    "accountId": "1234567",
    "webPropertyId": "UA-1234567-1",
    "internalWebPropertyId": "12345678",
    "profileName": "example.com",
    "tableId": "ga:12345678"
  },
  "containsSampledData": false,
  "columnHeaders": [
    {
      "name": "ga:pagePath",
      "columnType": "DIMENSION",
      "dataType": "STRING"
    },
    {
      "name": "ga:uniquePageviews",
      "columnType": "METRIC",
      "dataType": "INTEGER"
    }
  ],
  "totalsForAllResults": {
    "ga:uniquePageviews": "2773"
  },
  "rows": [
    [
      "/",
      "836"
    ],
    [
      "/node/1",
      "383"
    ],
    [
      "/node/2",
      "172"
    ],
    [
      "/contact",
      "138"
    ]
  ]
}
RESPONSE;
    $data->results = json_decode($data->response->data);
    $data->queryPath = 'https://www.example.com/analytics/v3/o/oauth2/token';
    $data->fromCache = TRUE;
    $data->accessToken = md5(time());
    $data->refreshToken = sha1(time());
    $data->expiresAt = time() + 60;

    return $data;
  }

}
