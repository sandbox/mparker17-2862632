<?php

namespace Drupal\ga_popular_nodes;

/**
 * An error that can be thrown when we can't authenticate with GA.
 */
class GaAuthenticationError extends \RuntimeException {}
